﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Question6
{
    class Quest
    {
        public IReadOnlyList<Soldier> Soldiers { get; }
        public Environment Environment { get; }

        public Quest(IReadOnlyList<Soldier> soldiers, Environment environment)
        {
            Soldiers = soldiers;
            Environment = environment;
        }

        public double ComputePrice()
        {
            return Soldiers.Sum(GetRate) * 10;
        }

        public double GetRate(Soldier soldier)
        {

            return soldier switch
            {
                case ElvenSoldier Soldier :
                    if(Soldier.Faction == "Forest" && Envirronement == Forest)
                        return 0.8;
                    if(Soldier.Faction == "Forest" && Envirronement == Mine)
                        return 1.2;
                    if(Soldier.Faction == "Forest")
                        return 1;
                    if(Soldier.Faction == "High")
                        return 1 + (0.2 * Soldiers.getDwarfCount());
                    break;
                case DwarfSoldier Soldier :
                     if(Soldier.BeardLength < 2)
                        return 0.7;
                     if(Soldier.BeardLength => 2 && Soldier.BeardLength < 5)
                        return 0.8;
                     if(Soldier.BeardLength => 5 && Soldier.BeardLength < 10)
                        return 1;
                     if(Soldier.BeardLength => 10)
                        return 1.2;
                     break;

                case KoboldGroup Soldier :
                       return 1 + (0.8 * (Soldiers.getKoboltCount() - 1));
                       break;
            };
        }
    }

}
