﻿
namespace Question6
{
    enum Environment
    {
        Prairie,
        Forest,
        Mountain,
        Mine,
    }

    class Soldier
    {
        public string Name { get; set; }
    }

    enum Faction
    {
        Forest,
        High
    }

    class ElvenSoldier : Soldier
    {
        public Faction Faction { get; set; }
    }

    class DwarfSoldier : Soldier
    {
        public double BeardLength { get; set; }
    }

    class KoboldGroup : Soldier
    {
        public int GroupSize { get; set; }
    }
}
