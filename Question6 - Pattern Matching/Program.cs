﻿using System;
using System.Collections.Generic;

namespace Question6
{
    class Program
    {
        static void Main()
        {
            var soldiers = new List<Soldier>
            {
                new ElvenSoldier() 
                { 
                    Name = "Legolas", 
                    Faction = Faction.Forest
                },

                new ElvenSoldier() 
                { 
                    Name = "Teclis", 
                    Faction = Faction.High 
                },

                new DwarfSoldier()
                {
                    Name = "Gimli",
                    BeardLength = 2.5
                },

                new KoboldGroup()
                {
                    Name = "Maxens",
                    GroupSize = 3
                },

                new KoboldGroup()
                {
                    Name = "Deriks",
                    GroupSize = 2
                }
            };

            var quest = new Quest(soldiers, Environment.Forest);
            foreach (var soldier in quest.Soldiers)
            {
                Console.WriteLine($"Rate for {soldier.Name} : {quest.GetRate(soldier)}");
            }

            Console.WriteLine($"Price : {quest.ComputePrice()}");
        }
    }
}
